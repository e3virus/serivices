/**
 * Created by remi on 17/01/15.
 */
(function() {

	var uploadfiles = document.querySelector('#uploadfiles');
	uploadfiles.addEventListener('change', function() {
		var files = this.files;
		for (var i = 0; i < files.length; i++) {
			uploadFile(this.files[i]);
		}

	}, false);

	function callbackFunction(xmlhttp) 
	{
	    alert(xmlhttp.responseXML);
	}
	
	/**
	 * Upload a file
	 * 
	 * @param file
	 */
	function uploadFile(file) {
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function() {
			console.log(reader.result);
		};
		reader.onerror = function(error) {
			console.log('Error: ', error);
		};

		var urlvariable;

	    urlvariable = "text";

	    var ItemJSON;

	    //ItemJSON = '[  {    "Id": 1,    "ProductID": "1",    "Quantity": 1,  },  {    "Id": 1,    "ProductID": "2",    "Quantity": 2,  }]';
	    ItemJSON = '{ "content" : "' + reader.result + '" , "fname": "filename1"';

	    URL = "https://jsonplaceholder.typicode.com/posts/1";  //Your URL

	    var xmlhttp = new XMLHttpRequest();
	    xmlhttp.onreadystatechange = callbackFunction(xmlhttp);
	    xmlhttp.open("GET", URL, false);
	    xmlhttp.setRequestHeader("Content-Type", "application/json");
	   // xmlhttp.setRequestHeader('Authorization', 'Basic ' + window.btoa('apiusername:apiuserpassword')); //in prod, you should encrypt user name and password and provide encrypted keys here instead 
	    xmlhttp.onreadystatechange = callbackFunction(xmlhttp);
	    //xmlhttp.send(ItemJSON);
	    xmlhttp.send();
	    alert(xmlhttp.responseText);
		
		
		
		//var url = "../server/index.php";
		//var xhr = new XMLHttpRequest();
		//var fd = new FormData();
		//xhr.open("POST", url, true);
		//xhr.onreadystatechange = function() {
		//	if (xhr.readyState == 4 && xhr.status == 200) {
				// Every thing ok, file uploaded
		//		console.log(xhr.responseText); // handle response.
		//	}
		};
		//fd.append('uploaded_file', file);
		//xhr.send(fd);
	//}
}());